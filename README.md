# OpenML dataset: Pima-Indians-Diabetes-Dataset

https://www.openml.org/d/43483

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The unprocessed dataset was acquired from UCI Machine Learning organisation. This dataset is preprocessed by me, originally from the National Institute of Diabetes and Digestive and Kidney Diseases. The objective of the dataset is to accurately predict whether or not, a patient has diabetes, based on multiple features included in the dataset. I've achieved an accuracy metric score of 92.86  with Random Forest Classifier using this dataset. I've even developed a web-service Diabetes Prediction System using that trained model. You can explore the Exploratory Data Analysis notebook to better understand the data.

Attributes Normal Value Range

Glucose: Glucose ( 140) = Normal, Glucose (140-200) = Pre-Diabetic, Glucose ( 200) = Diabetic
BloodPressure: B.P ( 60) = Below Normal, B.P (60-80) = Normal, B.P (80-90) = Stage 1 Hypertension, B.P (90-120) = Stage 2 Hypertension, B.P ( 120) = Hypertensive Crisis
SkinThickness: SkinThickness ( 10) = Below Normal, SkinThickness (10-30) = Normal, SkinThickness ( 30) = Above Normal
Insulin: Insulin ( 200) = Normal, Insulin ( 200) = Above Normal
BMI: BMI ( 18.5) = Underweight, BMI (18.5-25) = Normal, BMI (25-30) = Overweight, BMI ( 30) = Obese

Acknowledgements
J. W. Smith, J. E. Everhart, W. C. Dickson, W. C. Knowler and R. S. Johannes, "Using the ADAP Learning Algorithm to Forecast the Onset of Diabetes Mellitus" in Proc. of the Symposium on Computer Applications and Medical Care, pp. 261-265. IEEE Computer Society Press. 1988.

Inspiration
Multiple models were trained on the original dataset but only Random Forest Classifier was able to score an accuracy metric of 78.57  but with this new preprocessed dataset an accuracy metric score of 92.86  was achieved. Can you build a machine learning model that can accurately predict whether a patient has diabetes or not? and can you achieve an accuracy metric score even higher than 92.86  without overfitting the model?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43483) of an [OpenML dataset](https://www.openml.org/d/43483). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43483/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43483/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43483/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

